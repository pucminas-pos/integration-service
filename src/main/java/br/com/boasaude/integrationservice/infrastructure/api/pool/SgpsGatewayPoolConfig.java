package br.com.boasaude.integrationservice.infrastructure.api.pool;

import feign.Client;
import feign.Request;
import feign.httpclient.ApacheHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;

public class SgpsGatewayPoolConfig {

    @Value("${application.sgps-gateway.pool.maxConn}")
    private Integer maxConn;

    @Value("${application.sgps-gateway.pool.maxRoute}")
    private Integer maxRoute;

    @Value("${application.sgps-gateway.pool.connTimeout}")
    private Integer connTimeout;

    @Value("${application.sgps-gateway.pool.readTimeout}")
    private Integer readTimeout;

    @Bean
    public Request.Options options() {
        return new Request.Options(connTimeout, readTimeout);
    }

    @Bean
    public Client poolConfig() {
        return new ApacheHttpClient(
                HttpClientBuilder.create().setMaxConnPerRoute(maxRoute).setMaxConnTotal(maxConn).build());
    }
}
