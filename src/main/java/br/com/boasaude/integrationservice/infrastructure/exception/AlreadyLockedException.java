package br.com.boasaude.integrationservice.infrastructure.exception;

public class AlreadyLockedException extends RuntimeException {

    public AlreadyLockedException() {
    }

    public AlreadyLockedException(String message) {
        super(message);
    }

    public AlreadyLockedException(String message, Throwable cause) {
        super(message, cause);
    }

    public AlreadyLockedException(Throwable cause) {
        super(cause);
    }
}
