package br.com.boasaude.integrationservice.infrastructure.exception;

import org.springframework.http.HttpStatus;

public class SoGatewayIntegrationException extends IntegrationException {

    public SoGatewayIntegrationException(String message, HttpStatus httpStatus) {
        super(message, httpStatus);
    }
}
