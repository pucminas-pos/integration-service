package br.com.boasaude.integrationservice.infrastructure.exception;

public class MigrationToTheSamePlanException extends RuntimeException {

    public MigrationToTheSamePlanException() {
    }

    public MigrationToTheSamePlanException(String message) {
        super(message);
    }

    public MigrationToTheSamePlanException(String message, Throwable cause) {
        super(message, cause);
    }

    public MigrationToTheSamePlanException(Throwable cause) {
        super(cause);
    }
}
