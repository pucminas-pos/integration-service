package br.com.boasaude.integrationservice.infrastructure.exception;

import org.springframework.http.HttpStatus;

public class SgpsGatewayIntegrationException extends IntegrationException {

    public SgpsGatewayIntegrationException(String message, HttpStatus httpStatus) {
        super(message, httpStatus);
    }
}
