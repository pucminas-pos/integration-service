package br.com.boasaude.integrationservice.infrastructure.exception;

import org.springframework.http.HttpStatus;

public class IntegrationGatewayException extends IntegrationException {

    public IntegrationGatewayException(String message, HttpStatus httpStatus) {
        super(message, httpStatus);
    }
}
