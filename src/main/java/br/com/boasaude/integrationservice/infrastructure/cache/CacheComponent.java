package br.com.boasaude.integrationservice.infrastructure.cache;

import br.com.boasaude.integrationservice.domain.model.internal.Cache;
import lombok.RequiredArgsConstructor;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public final class CacheComponent {

    private static final String SEPARATOR = ":";

    private final RedisTemplate<String, Object> redisTemplate;


    public Object get(String prefix, String key) {
        return redisTemplate.opsForHash().get(prefix + SEPARATOR + key, key);
    }

    public void save(Cache cache) {
        final String cacheKey = cache.getPrefix() + SEPARATOR + cache.getKey();
        redisTemplate.opsForHash().putIfAbsent(cacheKey, cache.getKey(), cache.getObject());
        redisTemplate.expire(cacheKey, cache.getTtl(), cache.getTimeUnit());
    }
}
