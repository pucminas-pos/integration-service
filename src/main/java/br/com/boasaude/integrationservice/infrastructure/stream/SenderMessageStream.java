package br.com.boasaude.integrationservice.infrastructure.stream;

import br.com.boasaude.integrationservice.domain.model.internal.StreamRecordType;
import com.amazonaws.services.kinesis.AmazonKinesis;
import com.amazonaws.services.kinesis.model.PutRecordRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

@Slf4j
@Component
@RequiredArgsConstructor
public class SenderMessageStream {

    private final AmazonKinesis amazonKinesis;
    private final ObjectMapper objectMapper;

    @Async
    public void sendToStream(String streamName, Object object, StreamRecordType type) {
        try {
            PutRecordRequest putRecordRequest = new PutRecordRequest();
            putRecordRequest.setStreamName(streamName);

            String jsonObject = objectMapper.writeValueAsString(object);
            putRecordRequest.setData(ByteBuffer.wrap(jsonObject.getBytes(StandardCharsets.UTF_8)));
            putRecordRequest.setPartitionKey(type.name());
            amazonKinesis.putRecord(putRecordRequest);

        } catch (Exception ex) {
            log.error("error", ex);
        }
    }
}
