package br.com.boasaude.integrationservice.infrastructure.api;

import br.com.boasaude.integrationservice.domain.model.external.sgpsgateway.*;
import br.com.boasaude.integrationservice.domain.model.internal.ExceptionMessage;
import br.com.boasaude.integrationservice.infrastructure.api.pool.SgpsGatewayPoolConfig;
import br.com.boasaude.integrationservice.infrastructure.exception.IntegrationException;
import com.google.gson.Gson;
import feign.Response;
import feign.codec.ErrorDecoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.List;

@FeignClient(value = "sgpsgateway-api", url = "${application.sgps-gateway.url}",
        configuration = {SgpsGatewayAPI.SgpsGatewayErrorDecoder.class, SgpsGatewayPoolConfig.class})
public interface SgpsGatewayAPI {

    @PostMapping("associates/login")
    Associate login(@RequestBody LoginRequest loginRequest);

    @PostMapping("associates")
    Associate save(@RequestBody User user);

    @GetMapping("associates/email")
    Associate findByEmail(@RequestParam String email);

    @GetMapping("plans")
    List<Plan> findAllPlans();

    @GetMapping("plans/{id}")
    Plan findPlanById(@PathVariable("id") Long id);

    @GetMapping("plans/filter")
    Plan findPlanByNameAndCategoryAndType(@RequestParam("name") PlanName name,
                                          @RequestParam("category") PlanCategory category,
                                          @RequestParam("type") PlanType type);


    class SgpsGatewayErrorDecoder implements ErrorDecoder {

        @Autowired
        public Gson gson;

        @Override
        public Exception decode(String method, Response response) {
            final HttpStatus status = HttpStatus.valueOf(response.status());

            if (status == HttpStatus.INTERNAL_SERVER_ERROR)
                return new IntegrationException("integration error", HttpStatus.INTERNAL_SERVER_ERROR);
            try {
                Reader reader = response.body().asReader(StandardCharsets.UTF_8);
                ExceptionMessage exceptionMessage = gson.fromJson(reader, ExceptionMessage.class);
                return new IntegrationException(exceptionMessage.getMessage(), status);

            } catch (Exception ex) {
                return new IntegrationException("integration error", HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }
}
