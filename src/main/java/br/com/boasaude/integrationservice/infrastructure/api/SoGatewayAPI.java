package br.com.boasaude.integrationservice.infrastructure.api;

import br.com.boasaude.integrationservice.domain.model.external.sogateway.Reply;
import br.com.boasaude.integrationservice.domain.model.external.sogateway.Request;
import br.com.boasaude.integrationservice.domain.model.internal.ExceptionMessage;
import br.com.boasaude.integrationservice.infrastructure.api.pool.SoGatewayPoolConfig;
import br.com.boasaude.integrationservice.infrastructure.exception.IntegrationException;
import br.com.boasaude.integrationservice.infrastructure.exception.RequestCannotBeModifiedException;
import br.com.boasaude.integrationservice.infrastructure.exception.RequestNotFoundException;
import br.com.boasaude.integrationservice.infrastructure.exception.SoGatewayIntegrationException;
import com.google.gson.Gson;
import feign.Response;
import feign.codec.ErrorDecoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.Reader;
import java.nio.charset.StandardCharsets;

@FeignClient(value = "sogateway-api", url = "${application.so-gateway.url}",
        configuration = {SoGatewayAPI.SoGatewayErrorDecoder.class, SoGatewayPoolConfig.class})
public interface SoGatewayAPI {

    @PostMapping("requests")
    Request save(@RequestBody Request request);

    @PostMapping("replies")
    Reply save(@RequestBody Reply reply);

    @GetMapping("requests/{id}")
    Request findById(@PathVariable("id") Long id);

    @PatchMapping("requests/status/{id}")
    Request changeStatus(@PathVariable("id") Long id, @RequestBody Request request);


    class SoGatewayErrorDecoder implements ErrorDecoder {

        @Autowired
        public Gson gson;

        @Override
        public Exception decode(String method, Response response) {
            final HttpStatus status = HttpStatus.valueOf(response.status());

            if (status == HttpStatus.INTERNAL_SERVER_ERROR)
                return new IntegrationException("integration error", HttpStatus.INTERNAL_SERVER_ERROR);
            try {
                Reader reader = response.body().asReader(StandardCharsets.UTF_8);
                ExceptionMessage exceptionMessage = gson.fromJson(reader, ExceptionMessage.class);
                return new IntegrationException(exceptionMessage.getMessage(), status);

            } catch (Exception ex) {
                return new IntegrationException("integration error", HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }
}
