package br.com.boasaude.integrationservice.infrastructure.config;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.kinesis.AmazonKinesis;
import com.amazonaws.services.kinesis.AmazonKinesisClientBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AWSConfig {

    @Value("${amazon.region}")
    private Regions awsRegion;

    @Value("${amazon.kinesis.keep-alive}")
    private Boolean kinesisKeepAlive;

    @Value("${amazon.kinesis.max-connection}")
    private Integer kinesisMaxConnection;

    @Value("${amazon.kinesis.max-error-retry}")
    private Integer kinesisMaxErrorRetry;

    @Value("${amazon.kinesis.connection-timeout}")
    private Integer kinesisConnectionTimeout;

    @Value("${amazon.kinesis.client-timeout}")
    private Integer kinesisClientTimeout;

    @Value("${amazon.kinesis.socket-timeout}")
    private Integer kinesisSocketTimeout;


    @Bean
    public AWSCredentialsProvider awsCredentialsProvider() {
        return new AWSStaticCredentialsProvider(new BasicAWSCredentials("local", "local"));
    }

    @Bean("kinesisClient")
    public AmazonKinesis amazonKinesis(@Autowired AWSCredentialsProvider awsCredentialsProvider) {
        ClientConfiguration clientConfiguration = new ClientConfiguration();
        clientConfiguration.setUseTcpKeepAlive(kinesisKeepAlive);
        clientConfiguration.setMaxConnections(kinesisMaxConnection);
        clientConfiguration.setMaxErrorRetry(kinesisMaxErrorRetry);
        clientConfiguration.setConnectionTimeout(kinesisConnectionTimeout);
        clientConfiguration.setClientExecutionTimeout(kinesisClientTimeout);
        clientConfiguration.setSocketTimeout(kinesisSocketTimeout);
        clientConfiguration.setProxyHost("http://localhost:4566");

        AwsClientBuilder.EndpointConfiguration endpointConfiguration = new AwsClientBuilder.EndpointConfiguration("http://localhost:4566", "us-east-1");

        return AmazonKinesisClientBuilder.standard()
                .withCredentials(awsCredentialsProvider)
                .withEndpointConfiguration(endpointConfiguration)
                .withClientConfiguration(clientConfiguration)
                .build();
    }
}