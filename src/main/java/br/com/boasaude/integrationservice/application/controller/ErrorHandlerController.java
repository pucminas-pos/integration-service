package br.com.boasaude.integrationservice.application.controller;

import br.com.boasaude.integrationservice.infrastructure.exception.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@RestControllerAdvice
public class ErrorHandlerController {

    @ExceptionHandler(Exception.class)
    public ResponseEntity exception(Exception ex) {
        log.error("error: {}", ex);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(createErrorMap("unexpected error"));
    }

    @ExceptionHandler(LoginIncorrectException.class)
    public ResponseEntity loginIncorrectException(LoginIncorrectException ex) {
        log.error("error: ", ex);
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(createErrorMap(ex.getMessage()));
    }

    @ExceptionHandler(DataDuplicatedException.class)
    public ResponseEntity dataDuplicatedException(DataDuplicatedException ex) {
        log.error("error: ", ex);
        return ResponseEntity.status(HttpStatus.CONFLICT).body(createErrorMap(ex.getMessage()));
    }

    @ExceptionHandler(RequestCannotBeModifiedException.class)
    public ResponseEntity requestCannotBeModifiedException(RequestCannotBeModifiedException ex) {
        log.error("error: ", ex);
        return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(createErrorMap(ex.getMessage()));
    }

    @ExceptionHandler(RequestNotFoundException.class)
    public ResponseEntity requestNotFoundException(RequestNotFoundException ex) {
        log.error("error: ", ex);
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(createErrorMap(ex.getMessage()));
    }

    @ExceptionHandler(AlreadyLockedException.class)
    public ResponseEntity alreadyLockedException(AlreadyLockedException ex) {
        log.error("error: ", ex);
        return ResponseEntity.status(HttpStatus.LOCKED).body(createErrorMap(ex.getMessage()));
    }

    @ExceptionHandler(MigrationToTheSamePlanException.class)
    public ResponseEntity migrationToTheSamePlanException(MigrationToTheSamePlanException ex) {
        log.error("error: ", ex);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(createErrorMap(ex.getMessage()));
    }

    @ExceptionHandler(PlanNotFoundException.class)
    public ResponseEntity planNotFoundException(PlanNotFoundException ex) {
        log.error("error: ", ex);
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(createErrorMap(ex.getMessage()));
    }

    @ExceptionHandler(PlanStatusNotActiveException.class)
    public ResponseEntity planStatusNotActiveException(PlanStatusNotActiveException ex) {
        log.error("error: ", ex);
        return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(createErrorMap(ex.getMessage()));
    }

    @ExceptionHandler(IntegrationException.class)
    public ResponseEntity integrationException(IntegrationException ex) {
        log.error("error: ", ex);
        return ResponseEntity.status(ex.getHttpStatus()).body(createErrorMap(ex.getMessage()));
    }

    private final Map createErrorMap(String message) {
        Map<String, String> map = new HashMap<>();
        map.put("message", message);
        map.put("date", LocalDateTime.now().toString());
        return map;
    }
}
