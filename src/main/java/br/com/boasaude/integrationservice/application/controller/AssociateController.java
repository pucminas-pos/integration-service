package br.com.boasaude.integrationservice.application.controller;

import br.com.boasaude.integrationservice.domain.model.internal.PlanRequest;
import br.com.boasaude.integrationservice.domain.service.AssociateService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("associates")
public class AssociateController {

    private final AssociateService associateService;


    @PutMapping("plan/changing")
    public ResponseEntity changePlan(@RequestBody PlanRequest planRequest) {
        return ResponseEntity.ok(associateService.requestChangePlan(planRequest));
    }

    @DeleteMapping("plan/cancelation")
    public ResponseEntity cancelPlan(@RequestBody PlanRequest planRequest) {
        return ResponseEntity.ok(associateService.requestCancelPlan(planRequest));
    }
}
