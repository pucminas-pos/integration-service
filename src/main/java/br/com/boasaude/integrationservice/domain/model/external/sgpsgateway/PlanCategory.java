package br.com.boasaude.integrationservice.domain.model.external.sgpsgateway;

public enum PlanCategory {

    BASIC, INTERMEDIATE, VIP

}
