package br.com.boasaude.integrationservice.domain.model.external.sgpsgateway;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Plan {

    private Long id;
    private PlanName name;
    private PlanType type;
    private PlanCategory category;
}
