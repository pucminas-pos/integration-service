package br.com.boasaude.integrationservice.domain.service;

import br.com.boasaude.integrationservice.domain.model.external.sgpsgateway.Plan;
import br.com.boasaude.integrationservice.domain.model.internal.PlanRequest;
import br.com.boasaude.integrationservice.domain.model.internal.PlanResponse;

import java.util.List;

public interface AssociateService {

    PlanResponse requestChangePlan(PlanRequest planRequest);

    PlanResponse requestCancelPlan(PlanRequest planRequest);
}
