package br.com.boasaude.integrationservice.domain.model.external.sgpsgateway;

public enum PlanName {

    INDIVIDUAL, CORPORATE;

}
