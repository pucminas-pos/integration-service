package br.com.boasaude.integrationservice.domain.model.internal;

import br.com.boasaude.integrationservice.domain.model.external.sgpsgateway.PlanCategory;
import br.com.boasaude.integrationservice.domain.model.external.sgpsgateway.PlanName;
import br.com.boasaude.integrationservice.domain.model.external.sgpsgateway.PlanType;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Plan {

    private Long id;
    private PlanName name;
    private PlanType type;
    private PlanCategory category;

}
