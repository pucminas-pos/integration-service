package br.com.boasaude.integrationservice.domain.service;

import br.com.boasaude.integrationservice.domain.model.external.sgpsgateway.LoginRequest;
import br.com.boasaude.integrationservice.domain.model.external.sgpsgateway.User;

public interface UserService {

    User login(LoginRequest loginRequest);

    User save(User user);

    User findByEmail(String email);
}
