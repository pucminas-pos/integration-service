package br.com.boasaude.integrationservice.domain.service.impl;

import br.com.boasaude.integrationservice.domain.model.external.sgpsgateway.LoginRequest;
import br.com.boasaude.integrationservice.domain.model.external.sgpsgateway.User;
import br.com.boasaude.integrationservice.domain.service.UserService;
import br.com.boasaude.integrationservice.infrastructure.api.SgpsGatewayAPI;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final SgpsGatewayAPI sgpsGatewayAPI;


    @Override
    public User login(LoginRequest loginRequest) {
        return sgpsGatewayAPI.login(loginRequest);
    }

    @Override
    public User save(User user) {
        return sgpsGatewayAPI.save(user);
    }

    @Override
    public User findByEmail(String email) {
        return sgpsGatewayAPI.findByEmail(email);
    }
}
