package br.com.boasaude.integrationservice.domain.model.internal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.concurrent.TimeUnit;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Cache {

    private String prefix;
    private String key;
    private Object object;
    private TimeUnit timeUnit;
    private Integer ttl;
}
