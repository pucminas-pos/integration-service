package br.com.boasaude.integrationservice.domain.service.impl;

import br.com.boasaude.integrationservice.domain.model.external.sgpsgateway.Associate;
import br.com.boasaude.integrationservice.domain.model.external.sgpsgateway.Status;
import br.com.boasaude.integrationservice.domain.model.internal.*;
import br.com.boasaude.integrationservice.domain.service.AssociateService;
import br.com.boasaude.integrationservice.infrastructure.api.SgpsGatewayAPI;
import br.com.boasaude.integrationservice.infrastructure.cache.CacheComponent;
import br.com.boasaude.integrationservice.infrastructure.exception.AlreadyLockedException;
import br.com.boasaude.integrationservice.infrastructure.exception.MigrationToTheSamePlanException;
import br.com.boasaude.integrationservice.infrastructure.exception.PlanNotFoundException;
import br.com.boasaude.integrationservice.infrastructure.exception.PlanStatusNotActiveException;
import br.com.boasaude.integrationservice.infrastructure.stream.SenderMessageStream;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

@Slf4j
@Service
@RequiredArgsConstructor
public class AssociateServiceImpl implements AssociateService {

    private static final String prefix = "br.com.boasaude.integrationservice.plan";

    @Value("${amazon.kinesis.stream.plan}")
    private String kinesisStreamPlan;

    @Value("${spring.redis.ttl.plan}")
    private Integer ttlPlan;

    private final SgpsGatewayAPI sgpsGatewayAPI;
    private final SenderMessageStream senderMessageStream;
    private final CacheComponent cacheComponent;


    @Override
    public PlanResponse requestChangePlan(PlanRequest planRequest) {
       Object cache  = cacheComponent.get(prefix, planRequest.getEmail());
       if (Objects.nonNull(cache)) {
            throw new AlreadyLockedException("user has already requested changing plan");
       }

        Associate associate = sgpsGatewayAPI.findByEmail(planRequest.getEmail());

        if (Objects.isNull(associate.getPlan()))
            throw new PlanNotFoundException("associate does not have any plan");

        if (associate.getStatus() != Status.ACTIVE)
            throw new PlanStatusNotActiveException("associate plan status is not active");

        if ((associate.getPlan().getId() == planRequest.getNewPlan().getId())
                || (associate.getPlan().getName() == planRequest.getNewPlan().getName()
                && associate.getPlan().getCategory() == planRequest.getNewPlan().getCategory()
                && associate.getPlan().getType() == planRequest.getNewPlan().getType()))
            throw new MigrationToTheSamePlanException("migration plan to the same plan");

        planRequest.setCurrentPlan(Plan.builder()
                        .category(associate.getPlan().getCategory())
                        .type(associate.getPlan().getType())
                        .name(associate.getPlan().getName())
                .build());

        senderMessageStream.sendToStream(kinesisStreamPlan, planRequest, StreamRecordType.CHANGING_PLAN);
        cacheComponent.save(buildCache(planRequest.getEmail(), planRequest, ttlPlan));

        return PlanResponse.builder().newPlan(planRequest.getNewPlan()).date(LocalDateTime.now()).build();

    }

    @Override
    public PlanResponse requestCancelPlan(PlanRequest planRequest) {
        Object cache  = cacheComponent.get(prefix, planRequest.getEmail());
        if (Objects.nonNull(cache)) {
            throw new AlreadyLockedException("user has already requested canceling plan");
        }

        Associate associate = sgpsGatewayAPI.findByEmail(planRequest.getEmail());

        if (Objects.isNull(associate.getPlan()))
            throw new PlanNotFoundException("associate does not have any plan");

        if (associate.getStatus() != Status.ACTIVE)
            throw new PlanStatusNotActiveException("associate plan status is not active");

        planRequest.setCurrentPlan(Plan.builder()
                .category(associate.getPlan().getCategory())
                .type(associate.getPlan().getType())
                .name(associate.getPlan().getName())
                .build());

        senderMessageStream.sendToStream(kinesisStreamPlan, planRequest, StreamRecordType.CANCELING_PLAN);
        cacheComponent.save(buildCache(planRequest.getEmail(), planRequest, ttlPlan));

        return PlanResponse.builder().date(LocalDateTime.now()).build();
    }

    private Cache buildCache(String key, Object object, Integer ttl) {
        return Cache.builder()
                .prefix(prefix)
                .key(key)
                .object(object)
                .ttl(ttl)
                .timeUnit(TimeUnit.DAYS)
                .build();
    }
}
