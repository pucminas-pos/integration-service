package br.com.boasaude.integrationservice.domain.service.impl;

import br.com.boasaude.integrationservice.domain.model.external.sgpsgateway.Plan;
import br.com.boasaude.integrationservice.domain.model.internal.Cache;
import br.com.boasaude.integrationservice.domain.model.internal.PlanListCache;
import br.com.boasaude.integrationservice.domain.service.PlanService;
import br.com.boasaude.integrationservice.infrastructure.api.SgpsGatewayAPI;
import br.com.boasaude.integrationservice.infrastructure.cache.CacheComponent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

@Slf4j
@Service
@RequiredArgsConstructor
public class PlanServiceImpl implements PlanService {

    private static final String prefix = "br.com.boasaude.integrationservice.plan";

    @Value("${spring.redis.ttl.plan-all}")
    private Integer ttlAllPlan;

    private final SgpsGatewayAPI sgpsGatewayAPI;
    private final CacheComponent cacheComponent;


    @Override
    public List<Plan> findAll() {
        PlanListCache planListCache = (PlanListCache) cacheComponent.get(prefix, "all");
        if (Objects.isNull(planListCache)) {
            List<Plan> plans = sgpsGatewayAPI.findAllPlans();

            planListCache = PlanListCache.builder().plans(plans).build();

            Cache cache = buildCache("all", planListCache, ttlAllPlan);
            cacheComponent.save(cache);
        }

        return planListCache.getPlans();
    }

    private Cache buildCache(String key, Object object, Integer ttl) {
        return Cache.builder()
                .prefix(prefix)
                .key(key)
                .object(object)
                .ttl(ttl)
                .timeUnit(TimeUnit.DAYS)
                .build();
    }
}
