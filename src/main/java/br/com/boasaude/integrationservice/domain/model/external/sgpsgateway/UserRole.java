package br.com.boasaude.integrationservice.domain.model.external.sgpsgateway;

public enum UserRole {

    ADMIN, ASSOCIATE
}
