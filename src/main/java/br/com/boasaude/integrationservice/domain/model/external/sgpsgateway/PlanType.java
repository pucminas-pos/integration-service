package br.com.boasaude.integrationservice.domain.model.external.sgpsgateway;

public enum PlanType {

    MEDICAL, MEDICAL_ODONTOLOGY;

}
