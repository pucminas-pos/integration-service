package br.com.boasaude.integrationservice.domain.service.impl;

import br.com.boasaude.integrationservice.domain.model.external.sogateway.Reply;
import br.com.boasaude.integrationservice.domain.model.internal.StreamRecordType;
import br.com.boasaude.integrationservice.domain.service.ReplyService;
import br.com.boasaude.integrationservice.infrastructure.api.SoGatewayAPI;
import br.com.boasaude.integrationservice.infrastructure.stream.SenderMessageStream;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class ReplyServiceImpl implements ReplyService {

    @Value("${amazon.kinesis.stream.reply}")
    private String kinesisReplyStream;

    private final SoGatewayAPI soGatewayAPI;
    private final SenderMessageStream senderMessageStream;

    @Override
    public Reply save(Reply reply) {
        Reply persistedReply = soGatewayAPI.save(reply);
        senderMessageStream.sendToStream(kinesisReplyStream, persistedReply, StreamRecordType.REPLY);
        return persistedReply;
    }
}
