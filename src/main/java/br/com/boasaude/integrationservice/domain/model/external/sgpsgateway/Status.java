package br.com.boasaude.integrationservice.domain.model.external.sgpsgateway;

public enum Status {

    ACTIVE, SUSPENDED, INACTIVE

}
