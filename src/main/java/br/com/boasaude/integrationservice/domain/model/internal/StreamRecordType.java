package br.com.boasaude.integrationservice.domain.model.internal;

public enum StreamRecordType {

    REPLY, CHANGING_PLAN, CANCELING_PLAN, REQUEST

}
