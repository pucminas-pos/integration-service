package br.com.boasaude.integrationservice.domain.service;

import br.com.boasaude.integrationservice.domain.model.external.sogateway.Request;

public interface RequestService {

    Request save(Request request);

    Request changeStatus(Long id, Request request);

    Request findById(Long id);

}
