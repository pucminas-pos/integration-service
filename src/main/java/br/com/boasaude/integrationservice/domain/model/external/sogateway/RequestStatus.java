package br.com.boasaude.integrationservice.domain.model.external.sogateway;

public enum RequestStatus {

    OPENED, IN_PROGRESS, CLOSED;

}
