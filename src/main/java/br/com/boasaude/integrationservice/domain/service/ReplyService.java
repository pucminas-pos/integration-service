package br.com.boasaude.integrationservice.domain.service;

import br.com.boasaude.integrationservice.domain.model.external.sogateway.Reply;

public interface ReplyService {

    Reply save(Reply reply);

}
