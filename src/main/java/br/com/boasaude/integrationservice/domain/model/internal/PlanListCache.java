package br.com.boasaude.integrationservice.domain.model.internal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import br.com.boasaude.integrationservice.domain.model.external.sgpsgateway.Plan;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PlanListCache {

    private List<Plan> plans;

}
