package br.com.boasaude.integrationservice.domain.service.impl;

import br.com.boasaude.integrationservice.domain.model.external.sogateway.Request;
import br.com.boasaude.integrationservice.domain.model.internal.StreamRecordType;
import br.com.boasaude.integrationservice.domain.service.RequestService;
import br.com.boasaude.integrationservice.infrastructure.api.SoGatewayAPI;
import br.com.boasaude.integrationservice.infrastructure.stream.SenderMessageStream;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class RequestServiceImpl implements RequestService {

    @Value("${amazon.kinesis.stream.request}")
    private String kinesisRequestStream;

    private final SoGatewayAPI soGatewayAPI;
    private final SenderMessageStream senderMessageStream;


    @Override
    public Request save(Request request) {
        Request requestPersisted = soGatewayAPI.save(request);
        senderMessageStream.sendToStream(kinesisRequestStream, requestPersisted, StreamRecordType.REQUEST);
        return requestPersisted;
    }

    @Override
    public Request changeStatus(Long id, Request request) {
        return soGatewayAPI.changeStatus(id, request);
    }

    @Override
    public Request findById(Long id) {
        return soGatewayAPI.findById(id);
    }

}
