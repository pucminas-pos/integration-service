FROM maven:3.6.0-jdk-11-slim AS build
COPY src /home/integration-service/src
COPY pom.xml /home/integration-service
RUN mvn -f /home/integration-service/pom.xml clean package

FROM adoptopenjdk/openjdk11:alpine
COPY --from=build /home/integration-service/target/integration-service.jar /usr/local/lib/integration-service.jar
EXPOSE 8083
ENTRYPOINT ["java","-jar","/usr/local/lib/integration-service.jar"]